analytics README
==================

Getting Started
---------------

- place this directory within a new python virtualenv

- cd <directory containing this file>

- $venv/bin/python setup.py develop

- $venv/bin/populate_analytics development.ini

- $venv/bin/pserve development.ini

