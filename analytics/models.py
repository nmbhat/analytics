from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    backref
    )

from sqlalchemy.exc import IntegrityError

from zope.sqlalchemy import ZopeTransactionExtension
import json
from time import time as py_time

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class Session(Base):
    __tablename__ = 'sessions'
    id = Column(String(length=50), primary_key=True)
    token = Column(String(length=50), unique=True)
    profile_path = Column(String(length=100))
    super_properties = Column(String(length=800))
    time = Column(Integer)
    
    def __init__(self, id, token, profile_path=None, super_properties=None, time=int(py_time())):
        self.id = id
        self.token = token
        self.profile_path = profile_path
        self.super_properties = super_properties
        self.time = time #timestamp of last action taken by this session
    

class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    session_id = Column(String(length=50), ForeignKey('sessions.id'))
    name = Column(String(length=200))
    properties = Column(String(length=800))
    time = Column(Integer)

    session = relationship("Session", backref=backref('events',order_by=time))
    
    def __init__(self, session_id, time, name=None, properties=None):
        self.session_id = session_id
        self.name = name
        self.properties = properties
        self.time = time
        
def initialize_sql(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    try:
        dbsession = DBSession()
        session = Session('ccd23b8e3e29a34d03bea441aeebdf5c6ce9387', '0eb9basdc15ebd7d989ceb84c1fcc8eaf', None, None, 1329120150)
        event = Event('ccd23b8e3e29a34d03bea441aeebdf5c6ce9387', 1329120150, 'testt action', json.dumps({'test':'test2'}))
        dbsession.add(session)
        dbsession.add(event)
        dbsession.flush()
    except IntegrityError:
        # already created
        pass
