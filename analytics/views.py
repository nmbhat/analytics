from pyramid.view import view_config
from pretty_date import pretty_date

from .models import (
    DBSession,
    Session,
    Event
    )
from base64 import b64decode
from hashlib import md5
import json
from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import text
from time import time as py_time

EVENTS_PER_PAGE = 15
SESSIONS_PER_PAGE = 1
DAY_IN_SECONDS = 86400

@view_config(route_name='track', renderer='templates/track.pt')
def track(req):
    try:
        encoded = str(req.POST['data'])
        event_list = json.loads(b64decode(encoded))
    except:
        return {'params': -1}
    for event_obj in event_list:
        event_name = event_obj['event']
        event_properties = event_obj['properties']
        session_id = event_properties['distinct_id']
        db_session = DBSession()
        
        session = db_session.query(Session).filter_by(id=session_id).first()
        event = Event(session_id,event_properties['time'],event_name,json.dumps(event_properties))
        if session:
            super_properties = json.loads(session.super_properties)
            super_properties.update(event_properties)
            super_properties = json.dumps(super_properties)
            if event_properties.get('profile_path',None) and not session.profile_path:
                session.profile_path = event_properties['profile_path']
            if event_properties.get('time',None):
                session.time = event_properties['time']
        else:
            session = Session(session_id, event_properties.get('token',None), profile_path=event_properties.get('profile_path',None), super_properties=json.dumps(event_properties), time=event_properties.get('time',None))
        
        db_session.add(session)
        db_session.add(event)
        #db_session.flush()
    return {'params': 0}


def props_string(p): 
    """
    takes superprop dict and formats it
    into a string suitable for rendering 
    on the page
    """
    s = [k+': '+str(p[k])+', ' for k in p.keys()]
    s[-1] = s[-1][:-1]
    return "".join(s)

def event_dict(event, name):
    d = {
    'name': event.name,
    'properties': props_string(json.loads(event.properties)),
    'time': pretty_date(int(event.time)),
    'id': md5(str(event.time)+name).hexdigest(),
    'color': '#'+md5(event.name).hexdigest()[0:6]
    }
    return d

def session_dict(session, events, name):
    events = [event_dict(event, name) for event in events] #turn events into jinja2-friendly objects
    d = {
    'id': session.id,
    'name': name,
    'token': session.token,
    'profile_path': session.profile_path,
    'super_properties': str(session.super_properties),
    'events': events

    } 
    return d

def events_for_session(db_session, session_id, page=1, limit=EVENTS_PER_PAGE+1):
    return db_session.query(Event).filter_by(session_id=session_id).order_by(desc(Event.time)).limit(limit).offset((int(page)-1)*EVENTS_PER_PAGE).all()

def get_name(super_properties):
    return super_properties.get('mp_name_tag', None) or super_properties.get('profile_path', None) or super_properties.get('token', None)
    

@view_config(route_name='recent', renderer='templates/recent.jinja2')
def recent(req):
    page = req.GET.get('p',1)
    days = int(req.GET.get('d',14))
    db_session = DBSession()
    
    time_now = int(py_time())
    time_start = time_now - days*DAY_IN_SECONDS
    sessions = db_session.query(Session).filter(Session.time > time_start).order_by(desc(Session.time)).limit(SESSIONS_PER_PAGE+1).offset((int(page)-1)*SESSIONS_PER_PAGE).all()
    super_properties_list = [json.loads(s.super_properties) for s in sessions] #list of superprops as dicts for each session #todo delete?
    session_names = [get_name(sp) for sp in super_properties_list]
    eventLists = [events_for_session(db_session, s.id, page=1, limit=5) for s in sessions] #list of lists of event dicts.
    nextpage = str(int(page)+1) if len(sessions) > SESSIONS_PER_PAGE else False
    return {'sessions': [session_dict(sessions[i], eventLists[i], session_names[i]) for i in range(len(sessions[:SESSIONS_PER_PAGE]))], 'page':page, 'nextpage':nextpage, 'prevpage':str(int(page)-1)}

@view_config(route_name='user', renderer='templates/user.jinja2')
def user(req):
    try:
        s = req.GET['u']
    except:
        return {'user':{'name':'invalid'}}
    page = req.GET.get('p',1)
    db_session = DBSession()
    session = db_session.query(Session).filter_by(id=s).first()
    if session:
        super_properties = json.loads(session.super_properties)
        name = get_name(super_properties)
        events = events_for_session(db_session, session.id, page)
        nextpage = str(int(page)+1) if len(events) > EVENTS_PER_PAGE else False
        return {'session_id':s, 'page':page, 'username':name, 'user':session_dict(session,events[:EVENTS_PER_PAGE], name), 'nextpage':nextpage, 'prevpage':str(int(page)-1)}
        
    return {"username":'invalid'}