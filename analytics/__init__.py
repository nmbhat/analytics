from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import DBSession, initialize_sql

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    initialize_sql(engine)
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.add_jinja2_search_path("analytics:templates")
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('track', 'track')
    config.add_route('recent','recent')
    config.add_route('user','user')
    config.scan()
    return config.make_wsgi_app()

